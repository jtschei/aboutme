# aboutme

A little placeholder in GitLab while I explore the offerings

### Important Stuff

* My editor of choice is vi
* My SQL style has leading commas
```
sel 
 greatcolumn
,goodcolumn
,okcolumn
from awesomedb.fantastictable
where iscool = 'Y'
```
* Outside of SQL I use RStudio while I'm relearning with Python/Jupyter

### Observations

* I edited this inline and the editor seems ok, I like syntax formatting
* I authenticated using github which is interesting arrangement

### Curiousities

* How will company fare given github acquisition
* Is the office in Sacramento?